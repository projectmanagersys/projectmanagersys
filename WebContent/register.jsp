<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Register Form</title>
<style type="text/css">
.form-horizontal {
	width: 300px;
	margin: 0 auto;
	margin-top: 15%;
}
button{

}
</style>
</head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<body>
	<form class="form-horizontal" method="post" action="login/addNewUser">

		<div class="form-group">
			<label for="mailaddress" class="col-sm-3 control-label">E-Mail:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="mailadress" name="uid"
					required>
			</div>
		</div>

		<div class="form-group">
			<label for="password" class="col-sm-3 control-label">Name:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="name" name="name"
					required>
			</div>
		</div>

		<div class="form-group">
			<label for="password" class="col-sm-3 control-label">Surname:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="surname" name="surname"
					required>
			</div>
		</div>

		<div class="form-group">
			<label for="password" class="col-sm-3 control-label">Password:</label>
			<div class="col-sm-9">
				<input type="password" class="form-control" id="password"
					name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
					onchange="form.repassword.pattern = this.value;"
					title="Must contain at
				 least one number and one uppercase and lowercase letter, and at least 8 or more characters"
					required>


			</div>
		</div>
		<div class="form-group">
			<label for="password" class="col-sm-3 control-label">Re-Password:</label>
			<div class="col-sm-9">
				<input type="password" class="form-control" id="repassword"
					name="repassword" pattern="" required="">


			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-10">
				<span style="margin-left:28%;"><button type="submit" class="btn btn-primary">Register</button>
			</span></div>
		</div>
	</form>
</body>
</html>