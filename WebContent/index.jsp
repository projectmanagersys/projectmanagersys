<%@page import="bean.FullName"%>
<%@page import="javax.naming.directory.Attribute"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<style type="text/css">
body {
	font-weight: bold;
}

#genel {
	width: 800px;
	height: 800px;
}

#proje {
	width: 2000px;
	height: 300px;
	margin-top: 20px;
	margin-left: 80px;
	font-size: 14px;
	
	
}

.proje{
	width:300px;
	height:100px;
	text-align:center;
	display: flex;
    margin: 0 20px;
    background-color: rgb(255, 255, 255);
    padding: 45px 85px;
   float:left;
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    transition: all 150ms ease-in-out;
    cursor: pointer;
   
}
#menu {
	width: 800px;
	height: 50px;
}

#ekip {
	width: 800px;
	height: 200px;
	margin-left: 80px;
	color: red;
}

nav.navbar-findcond {
	background: #fff;
	border-color: #ccc;
	box-shadow: 0 0 2px 0 #ccc;
}

nav.navbar-findcond a {
	color: #f14444;
}

nav.navbar-findcond ul.navbar-nav a {
	color: #f14444;
	border-style: solid;
	border-width: 0 0 2px 0;
	border-color: #fff;index.jsp
}

nav.navbar-findcond ul.navbar-nav a:hover, nav.navbar-findcond ul.navbar-nav a:visited,
	nav.navbar-findcond ul.navbar-nav a:focus, nav.navbar-findcond ul.navbar-nav a:active
	{
	background: #fff;
}

nav.navbar-findcond ul.navbar-nav a:hover {
	border-color: #f14444;
}

nav.navbar-findcond li.divider {
	background: #ccc;
}

nav.navbar-findcond button.navbar-toggle {
	background: #f14444;
	border-radius: 2px;
}

nav.navbar-findcond button.navbar-toggle:hover {
	background: #999;
}

nav.navbar-findcond button.navbar-toggle>span.icon-bar {
	background: #fff;
}

nav.navbar-findcond ul.dropdown-menu {
	border: 0;
	background: #fff;
	border-radius: 4px;
	margin: 4px 0;
	box-shadow: 0 0 4px 0 #ccc;
}

nav.navbar-findcond ul.dropdown-menu>li>a {
	color: #444;
}

nav.navbar-findcond ul.dropdown-menu>li>a:hover {
	background: #f14444;
	color: #fff;
}

nav.navbar-findcond span.badge {
	background: #f14444;
	font-weight: normal;
	font-size: 11px;
	margin: 0 4px;
}

nav.navbar-findcond span.badge.new {
	background: rgba(255, 0, 0, 0.8);
	color: #fff;
}

</style>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
	integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
	integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
	crossorigin="anonymous"></script>
</head>
<body>



	<div id="Genel">
		<div id="menu">
			<nav class="navbar navbar-findcond navbar-fixed-top">



			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Proje Yönetim Sistemi</a>
				</div>
				<div class="collapse navbar-collapse" id="navbar">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-expanded="false"><i
								class="fa fa-fw fa-bell-o"></i> Bildirimler <span class="badge">0</span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#"><i class="fa fa-fw fa-tag"></i> <span
										class="badge">Music</span> sayfası <span class="badge">Video</span>
										sayfasında etiketlendi</a></li>
								<li><a href="#"><i class="fa fa-fw fa-thumbs-o-up"></i>
										<span class="badge">Music</span> sayfasında iletiniz beğenildi</a></li>
								<li><a href="#"><i class="fa fa-fw fa-thumbs-o-up"></i>
										<span class="badge">Video</span> sayfasında iletiniz beğenildi</a></li>
								<li><a href="#"><i class="fa fa-fw fa-thumbs-o-up"></i>
										<span class="badge">Game</span> sayfasında iletiniz beğenildi</a></li>
							</ul></li>
						<li class="active"><a href="#">Ana Sayfa <span
								class="sr-only">(current)</span></a></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-expanded="false"><%=request.getUserPrincipal().getName()%>
								<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Geri bildirim</a></li>
								<li><a href="#">Yardım</a></li>
								<li class="divider"></li>
								<li><a href="#">Ayarlar</a></li>
								<li><a href="logout.jsp">logout</a></li>
							</ul></li>
					</ul>
					<form class="navbar-form navbar-right search-form" role="search">
						<input type="text" class="form-control" placeholder="Search" />
						</form>
		</div>
			</div>
			</nav>
		</div>
		<div id="proje">
			<a href="#" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><h4 class="proje" >Proje Oluştur 
		</h4></a>
<a href="#" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">		
<h4 class="proje"><%=request.getParameter("bas")%> </h4></a>

<a href="#" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">		
<h4 class="proje"><%=request.getParameter("bas")%> </h4></a>			
			
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" style="text-align:center;"id="exampleModalLabel">Yeni Proje Oluştur</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="index.jsp" method="get">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label" >Başlık:</label>
            <input type="text" class="form-control"id="bas" name="bas">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Ekip</label><br>
           <select style="width:570px;height:30px">
  <option value="volvo">Hiçbiri</option>
  <option value="saab">Saab</option>
  <option value="mercedes">Mercedes</option>
  <option value="audi">Audi</option>
</select>
             <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <input type="submit" class="btn btn-primary" value="Send message"></input>
          </div>
        </form>
      </div>
     
      </div>
    </div>
  </div>
</div>
		</div>
		<div id="ekip">
		<a href="#" data-toggle="modal" data-target="#exampleModal1" data-whatever="@mdo"><h4 class="active"><h4>Ekip Oluştur</h4></a>
		<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" style="text-align:center;"id="exampleModalLabel">Yeni Ekip Oluştur</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Ad:</label>
            <input type="text" class="form-control" id="recipient-name">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Açıklama(İsteğe Bağlı)</label><br>
        <textarea class="form-control" id="message-text"></textarea>
            
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div>
    </div>
  </div>
</div>
		</div>

	</div>

</body>
</html>