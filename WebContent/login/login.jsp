<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome</title>
<style type="text/css">

.form-horizontal {
	width: 300px;
	margin: 0 auto;
	margin-top: 20%;
}
</style>

	<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
	
</head>



<form class="form-horizontal" method="post" action="j_security_check">

	<div class="form-group">
		<label for="mailaddress" class="col-sm-3 control-label">E-Mail:</label>
		<div class="col-sm-9">
			<input type="text" class="form-control" id="mailadress"
				name="j_username" required>
		</div>
	</div>
	<div class="form-group">
		<label for="password" class="col-sm-3 control-label">Password:</label>
		<div class="col-sm-9">
			<input type="password" class="form-control" id="password"
				name="j_password" required>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-10">
			<button type="submit" class="btn btn-primary">Login</button>
		<a href="register.jsp" class="btn btn-danger" role="button">Register</a>
			
		</div>
		
	</div>
  <c:if test="${succeed}"><div class="alert alert-success">
  <strong>Success!</strong> Membership successful.
</div></c:if>

	

	
</form>

</body>
</html>