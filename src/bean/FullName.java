package bean;

import java.util.Hashtable;

import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.NameParser;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

public class FullName {
	
	public static String getFullName(String name) throws NamingException {
		// TODO Auto-generated method stub
		// Principal principal = principalInstance.get();
		// String username = principal.getName();
		
		
		String uidName = null;
		Hashtable<String, String> env = new Hashtable<>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, "ldap://217.61.1.62:10389/dc=32bit,dc=com");

		DirContext context = new InitialDirContext(env);
		DirContext groupCtx = (DirContext) context.lookup("ou=groups");
		DirContext usersCtx = (DirContext) context.lookup("ou=users");

		NameParser nameParser = usersCtx.getNameParser("");
		NamingEnumeration<Binding> user = usersCtx.listBindings("");

		// NamingEnumeration<Binding> enumeration=context.listBindings("");
		while (user.hasMore()) {
			String bindingName = user.next().getName();
			Attributes groupAttributes = usersCtx.getAttributes(bindingName);
			javax.naming.directory.Attribute cn = groupAttributes.get("cn");
			javax.naming.directory.Attribute uid = groupAttributes.get("uid");
			
			uidName=(String) uid.get();
			
			//System.out.println(uidName);

			if (uidName.equals(name))
			{
			//System.out.println(cn.get());
				return (String) cn.get();
			
			}
			
			
			
			
			/*NamingEnumeration<?> members = uniqueMember.getAll();
			
			while (members.hasMore()) {
				String memberDN = members.next().toString();
				Name memberName = nameParser.parse(memberDN);
				DirContext member = (DirContext) usersCtx.lookup(memberName.get(1));
				Attributes memberAttributes = member.getAttributes("", new String[] { "uid", "cn" });
				// System.out.println(memberAttributes.get("cn").get());
			}

		}*/
	}
		return null;
		
	}}