package contoller;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;

public class AddUser {
public void addUser(String mail, String pass,String name, String surname) throws NamingException
{
	final Hashtable<Object, Object> env = new Hashtable<Object, Object>();
	DirContext dctx = null;
		String url = "ldap://217.61.1.62:10389";
		String conntype = "simple";
		String AdminDn = "uid=admin,ou=system";
		String password = "secret";

		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, url);
		env.put(Context.SECURITY_AUTHENTICATION, conntype);
		env.put(Context.SECURITY_PRINCIPAL, AdminDn);
		env.put(Context.SECURITY_CREDENTIALS, password);
		dctx = new InitialDirContext(env);
		// Create a container set of attributes
		
		Attribute userCn = new BasicAttribute("cn", name);
		Attribute userSn = new BasicAttribute("sn", surname);
		Attribute userUserPassword = new BasicAttribute("userPassword",pass);
		Attribute uid = new BasicAttribute("uid",mail);

		Attribute oc = new BasicAttribute("objectClass");
		oc.add("person");
		oc.add("organizationalPerson");
		oc.add("inetOrgPerson");
		Attributes entry = new BasicAttributes();
		entry.put(userSn);
		entry.put(uid);
		entry.put(userUserPassword);
		entry.put(oc);
		String entryDN = "cn="+name+" "+surname+",ou=users,dc=projectManager,dc=com";
		dctx.createSubcontext(entryDN, entry);
		String d="cn=Users,ou=groups,dc=projectManager,dc=com";
		
		Attributes attributes = dctx.getAttributes("cn=Users,ou=groups,dc=projectManager,dc=com");
		
		Attribute attribute = new BasicAttribute("uniqueMember", entryDN);
		ModificationItem[] item = new ModificationItem[1];
		item[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE, attribute);
		attributes.put(attribute);
		dctx.modifyAttributes(d, item);
		

		
		
		
		
		
}

}
