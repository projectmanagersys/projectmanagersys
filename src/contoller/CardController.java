package contoller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CardController
 */
@WebServlet("/ProjectCardController")
public class CardController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String index = "/index.jsp";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CardController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String veri=request.getParameter("bas");
		ArrayList<String>list= new ArrayList<String>();
		list.add(veri);
		ServletContext sc=getServletContext();
		sc.setAttribute("user",veri);
		//request.setAttribute("items", list);
		RequestDispatcher view = request.getRequestDispatcher("/viewProfile.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		
		
	}

}
